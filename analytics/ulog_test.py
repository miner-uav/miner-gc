import px4tools
import pandas
import os

log = px4tools.read_ulog('/data/PX4_Logs/22_26_29.ulg')
streamKeys = log.keys()
print(streamKeys)
d_accel = log['sensor_accel_0']
d_gyro = log['sensor_gyro_0']
px4tools.ulog.noise_analysis_sensor(d_accel, topic='sensor_accel_0',
                                    allan_args={'poly_order':3, 'min_intervals':9})
print("exit")