#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <ros/package.h>
#include "vector"
#include <boost/foreach.hpp>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "image_exporter");

    ros::NodeHandle n;

    ros::Rate loop_rate(10);
    std::string bag_path;

    ROS_INFO_STREAM("Parameters:");
    std::vector<std::string> keys;
    std::string value;
    n.getParamNames(keys);

    BOOST_FOREACH(std::string const key, keys)
    {
        n.getParam(key, value);
        ROS_INFO_STREAM("PARAM:" << key << ":=" << value);
    }

    if(!n.getParam("/image_exporter/bag_file", bag_path))
    {
        ROS_FATAL_STREAM("Parameter \"bag_file\" has to be supplied" << bag_path);
        exit( -1 );
    }


    if(ros::ok())
    {
        rosbag::Bag bag;
        bag.open(bag_path, rosbag::bagmode::Read);
        bag.close();
        ros::spinOnce();
        loop_rate.sleep();
    }

}
