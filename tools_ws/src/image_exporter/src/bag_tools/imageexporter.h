#ifndef IMAGEEXPORTER_H
#define IMAGEEXPORTER_H

#include <ros/package.h>

namespace bag_tools {

class ImageExporter
{
public:
    ImageExporter(std::string bagFilePath, std::string exportPath );
};
}
#endif // IMAGEEXPORTER_H
