/*
 * Copyright (c) 2011, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>

#include <QPainter>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QTimer>

#include <geometry_msgs/Twist.h>
#include <std_srvs/Empty.h>
#include <dji_sdk/DroneArmControl.h>
#include <dji_sdk/SDKControlAuthority.h>
#include "trajectory_plugin.h"


namespace rviz_plugin_traj_planner
{

// BEGIN_TUTORIAL
// Here is the implementation of the TeleopPanel class.  TeleopPanel
// has these responsibilities:
//
// - Act as a container for GUI elements DriveWidget and QLineEdit.
// - Publish command velocities 10 times per second (whether 0 or not).
// - Saving and restoring internal state from a config file.
//
// We start with the constructor, doing the standard Qt thing of
// passing the optional *parent* argument on to the superclass
// constructor, and also zero-ing the velocities we will be
// publishing.
TrajectoryEditorPlugin::TrajectoryEditorPlugin( QWidget* parent )
  : rviz::Panel( parent )
  , defaultHeight(2.0)
  , drone_armed(false)
  , dji_sdk_has_authority(false)
{
    // Next we lay out the "output topic" text entry field using a
    // QLabel and a QLineEdit in a QHBoxLayout.
    QHBoxLayout* topic_layout = new QHBoxLayout;
    topic_layout->addWidget( new QLabel( "Trajectory Height:" ));
    output_topic_editor_ = new QLineEdit;
    output_topic_editor_->setValidator(new QDoubleValidator());
    topic_layout->addWidget( output_topic_editor_ );

    //Table
    QHBoxLayout* table_layout = new QHBoxLayout;
    table_view_ = new QTableView(parent);
    table_view_->setSelectionBehavior(QAbstractItemView::SelectRows);
    table_view_->setSelectionMode(QAbstractItemView::SingleSelection);
    table_view_model_ = new TrajPointsDataModel(table_view_);
    table_view_->setModel( table_view_model_ );
    table_layout->addWidget(table_view_);

    //Buttons
    QHBoxLayout* buttons_layout = new QHBoxLayout();

    traj_delete_btn_= new QPushButton("Del Point");
    buttons_layout->addWidget(traj_delete_btn_);

    traj_reset_btn_ = new QPushButton("Reset Trajectory");
    buttons_layout->addWidget(traj_reset_btn_);

    traj_exit_teleop_btn_ = new QPushButton("Exit Teleop");
    buttons_layout->addWidget(traj_exit_teleop_btn_);

    drone_get_auth_btn_ = new QPushButton("GetAuthority");
    buttons_layout->addWidget(drone_get_auth_btn_);
    drone_release_auth_btn_ = new QPushButton("Release Authority");
    buttons_layout->addWidget(drone_release_auth_btn_);

    drone_arm_btn_ = new QPushButton("Arm");
    drone_arm_btn_->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 255, 0);"));
    buttons_layout->addWidget(drone_arm_btn_);

    traj_go_btn_ = new QPushButton("GO");
    traj_go_btn_->setStyleSheet(QString::fromUtf8("background-color: rgb(78, 154, 6);"));
    buttons_layout->addWidget(traj_go_btn_);


    // Lay out the topic field above the control widget.
    QVBoxLayout* main_layout = new QVBoxLayout;
    main_layout->addLayout( topic_layout );
    main_layout->addLayout(table_layout);
    main_layout->addLayout(buttons_layout);
    setLayout( main_layout );

    // Next we make signal/slot connections.
    connect( output_topic_editor_, SIGNAL( editingFinished() ), this, SLOT( updateDefaultHeight() ));
    connect(traj_reset_btn_, SIGNAL (clicked()),this, SLOT (reset_traj_handler()));
    connect(traj_delete_btn_, SIGNAL (clicked()),this, SLOT (deleteSelectedPoint()));
    connect(traj_go_btn_, SIGNAL (clicked()),this, SLOT (traj_execute()));
    connect(traj_exit_teleop_btn_, SIGNAL (clicked()),this, SLOT (exit_teleop()));
    connect(drone_arm_btn_, SIGNAL (clicked()),this, SLOT (drone_arm()));
    connect(drone_get_auth_btn_, SIGNAL (clicked()),this, SLOT (gain_sdk_authority()));
    connect(drone_release_auth_btn_, SIGNAL (clicked()),this, SLOT (release_sdk_authority()));

    connect(table_view_model_, SIGNAL (updateTrajectory()),this, SLOT (displayTrajectory()));
    // Start the timer.

    rviz_nav_goal_sub_ = nh_.subscribe<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1, &TrajectoryEditorPlugin::rvizPointHndler , this);
    dji_flight_status_sub_ = nh_.subscribe<std_msgs::UInt8>("/dji_sdk/flight_status", 1, &TrajectoryEditorPlugin::dji_flight_status , this);

    srv_vis_traj_client_ = nh_.serviceClient<std_srvs::Empty>("/miner/visualize_path");
    srv_execute_traj_client_ = nh_.serviceClient<std_srvs::Empty>("/miner/execute_path");
    srv_exit_teleop_client_ = nh_.serviceClient<std_srvs::Empty>("/miner/back_to_position_hold");
    srv_drone_arm_client_ = nh_.serviceClient<dji_sdk::DroneArmControl>("/dji_sdk/drone_arm_control");
    srv_sdk_authority_client_ = nh_.serviceClient<dji_sdk::SDKControlAuthority>("/dji_sdk/sdk_control_authority");
}



void TrajectoryEditorPlugin::gain_sdk_authority()
{

    dji_sdk::SDKControlAuthorityRequest request;
    dji_sdk::SDKControlAuthorityResponse response;
    request.control_enable = 1;

    if(srv_sdk_authority_client_.call(request, response))
    {
        if(response.result)
        {
            dji_sdk_has_authority = true;
        }
    }
    updateSdkAuthorityButtons();

}

void TrajectoryEditorPlugin::release_sdk_authority()
{

    dji_sdk::SDKControlAuthorityRequest request;
    dji_sdk::SDKControlAuthorityResponse response;
    request.control_enable = 0;

    if(srv_sdk_authority_client_.call(request, response))
    {
        if(response.result)
        {
            dji_sdk_has_authority = false;
        }
    }
    updateSdkAuthorityButtons();
}

void TrajectoryEditorPlugin::exit_teleop()
{

    std_srvs::Empty::Request empty_request;
    std_srvs::Empty::Response empty_response;
    srv_exit_teleop_client_.call(empty_request, empty_response);
}

void TrajectoryEditorPlugin::updateSdkAuthorityButtons()
{
    if(dji_sdk_has_authority)
    {
        drone_get_auth_btn_->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 0, 0);"));
        drone_release_auth_btn_->setStyleSheet(QString::fromUtf8(""));
    }
    else
    {
        drone_get_auth_btn_->setStyleSheet(QString::fromUtf8(""));
        drone_release_auth_btn_->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 0, 0);"));
    }
}

void TrajectoryEditorPlugin::updateArmButton()
{
    if(drone_armed)
    {
        drone_arm_btn_->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 0, 0);"));
        drone_arm_btn_->setText("Disarm");
    }
    else
    {
        drone_arm_btn_->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 255, 0);"));
        drone_arm_btn_->setText("Arm");
    }
}

void TrajectoryEditorPlugin::drone_arm()
{
    dji_sdk::DroneArmControlRequest request;
    dji_sdk::DroneArmControlResponse response;
    request.arm = (!drone_armed);
    srv_drone_arm_client_.call(request, response);
    if(response.result)
    {
        drone_armed = !drone_armed;
    }
    updateArmButton();
}

void TrajectoryEditorPlugin::deleteSelectedPoint()
{
    auto selModel = table_view_->selectionModel();
    if(selModel && selModel->hasSelection())
    {
        auto items = selModel->selectedIndexes();
        int row = items[0].row();
        table_view_model_->removeRows(row, 1, QModelIndex());
    }
}

void TrajectoryEditorPlugin::displayTrajectory()
{
    wn_params.resetTraj();
    table_view_model_->exportData(wn_params.northings, wn_params.eastings, wn_params.heights, wn_params.headings);
    wn_params.writeParams();
    std_srvs::Empty::Request empty_request;
    std_srvs::Empty::Response empty_response;
    srv_vis_traj_client_.call(empty_request, empty_response);
}

void TrajectoryEditorPlugin::dji_flight_status(const std_msgs::UInt8ConstPtr &statusPtr)
{
    if(statusPtr->data ==0)
    {
        if(drone_armed)
        {
            drone_armed = false;
            updateArmButton();
        }
    }
    else
    {
        if(!drone_armed)
        {
            drone_armed = true;
            updateArmButton();
        }
    }
}

void TrajectoryEditorPlugin::rvizPointHndler(const geometry_msgs::PoseStampedConstPtr &posePtr)
{
    if(posePtr->header.frame_id != wn_params.frame_id)
    {
        geometry_msgs::PoseStamped new_pose;

        tf_listner_.transformPose(wn_params.frame_id, *posePtr, new_pose);
        table_view_model_->addTrajPoint(
                    new_pose.pose.position.x,
                    new_pose.pose.position.y,
                    defaultHeight,
                    0.0);
    }
    else
    {
        table_view_model_->addTrajPoint(
                    posePtr->pose.position.x,
                    posePtr->pose.position.y,
                    defaultHeight,
                    0.0);
    }
    displayTrajectory();
}

void TrajectoryEditorPlugin::reset_traj_handler()
{
    table_view_model_->clearData();
}

void TrajectoryEditorPlugin::traj_execute()
{
    wn_params.writeParams();
    std_srvs::Empty::Request empty_request;
    std_srvs::Empty::Response empty_response;

    srv_execute_traj_client_.call(empty_request, empty_response);
}

// Read the topic name from the QLineEdit and call setTopic() with the
// results.  This is connected to QLineEdit::editingFinished() which
// fires when the user presses Enter or Tab or otherwise moves focus
// away.
void TrajectoryEditorPlugin::updateDefaultHeight()
{
    defaultHeight = output_topic_editor_->text().toDouble();
    Q_EMIT configChanged();

}


// Save all configuration data from this panel to the given
// Config object.  It is important here that you call save()
// on the parent class so the class id and panel name get saved.
void TrajectoryEditorPlugin::save( rviz::Config config ) const
{
  rviz::Panel::save( config );
  config.mapSetValue( "DefaultHeight", defaultHeight );
}

// Load all configuration data for this panel from the given Config object.
void TrajectoryEditorPlugin::load( const rviz::Config& config )
{
    rviz::Panel::load( config );
    float defHeight;
    if( config.mapGetFloat("DefaultHeight", &defHeight ))
    {
        defaultHeight = defHeight;
        output_topic_editor_->setText( QString::number(defHeight));
    }
}

} // end namespace rviz_plugin_traj_planner

// Tell pluginlib about this class.  Every class which should be
// loadable by pluginlib::ClassLoader must have these two lines
// compiled in its .cpp file, outside of any namespace scope.
#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(rviz_plugin_traj_planner::TrajectoryEditorPlugin,rviz::Panel )
// END_TUTORIAL
