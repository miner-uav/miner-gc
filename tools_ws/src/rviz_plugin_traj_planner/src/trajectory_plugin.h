/*
 * Copyright (c) 2011, Willow Garage, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Willow Garage, Inc. nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef TRAJECTORY_PLUGIN_H
#define TRAJECTORY_PLUGIN_H

#ifndef Q_MOC_RUN
#include "trajpointsdatamodel.h"

# include <ros/ros.h>

# include <rviz/panel.h>
#endif
#include <QPushButton>
#include <QTableView>
#include <QLineEdit>
#include <geometry_msgs/PoseStamped.h>
#include <std_msgs/UInt8.h>
#include <tf/transform_listener.h>

namespace rviz_plugin_traj_planner
{

class WaypointNavigatorParams
{
public:
    std::vector<double> eastings;
    std::vector<double> northings;
    std::vector<double> heights;
    std::vector<double> headings;
    bool intermediate_poses = false;
    std::string frame_id = "mission";
    //std::string base_path = "/miner/waypoint_navigator_node";


    WaypointNavigatorParams():
    nh_("/miner/waypoint_navigator_node/")
    ,frame_id("mission")
    ,intermediate_poses(false)
    {
    }

    void readParams()
    {
        std::string name_space = nh_.getNamespace();
        nh_.getParam("northin", northings);
        nh_.getParam("easting", eastings);
        nh_.getParam("height", heights);
        nh_.getParam("heading", headings);
        //nh_.getParam("intermediate_poses", intermediate_poses);
        //nh_.getParam("frame_id", frame_id);

    }

    void writeParams()
    {
        nh_.setParam("northing", northings);
        nh_.setParam("easting", eastings);
        nh_.setParam("height", heights);
        nh_.setParam("heading", headings);
        nh_.setParam("intermediate_poses", intermediate_poses);
        nh_.setParam("frame_id", frame_id);
    }

    void resetTraj()
    {
        northings.clear();
        eastings.clear();
        heights.clear();
        headings.clear();
    }



protected:
    ros::NodeHandle nh_;

};

// BEGIN_TUTORIAL
// Here we declare our new subclass of rviz::Panel.  Every panel which
// can be added via the Panels/Add_New_Panel menu is a subclass of
// rviz::Panel.
//
// TeleopPanel will show a text-entry field to set the output topic
// and a 2D control area.  The 2D control area is implemented by the
// DriveWidget class, and is described there.
class TrajectoryEditorPlugin: public rviz::Panel
{
// This class uses Qt slots and is a subclass of QObject, so it needs
// the Q_OBJECT macro.
Q_OBJECT
public:
    // QWidget subclass constructors usually take a parent widget
    // parameter (which usually defaults to 0).  At the same time,
    // pluginlib::ClassLoader creates instances by calling the default
    // constructor (with no arguments).  Taking the parameter and giving
    // a default of 0 lets the default constructor work and also lets
    // someone using the class for something else to pass in a parent
    // widget as they normally would with Qt.
    TrajectoryEditorPlugin( QWidget* parent = 0 );

    // Now we declare overrides of rviz::Panel functions for saving and
    // loading data from the config file.  Here the data is the
    // topic name.
    virtual void load( const rviz::Config& config );
    virtual void save( rviz::Config config ) const;

    // Next come a couple of Qt slots.
protected Q_SLOTS:

    // updateTopic() reads the topic name from the QLineEdit and calls
    // setTopic() with the result.
    void updateDefaultHeight();
    void exit_teleop();
    void drone_arm();
    void gain_sdk_authority();
    void release_sdk_authority();

    void reset_traj_handler();
    void traj_execute();
    void displayTrajectory();
    void deleteSelectedPoint();


    // Then we finish up with protected member variables.
protected:
    //TrajectoryResetButton
    QPushButton *traj_reset_btn_;

    QPushButton *traj_delete_btn_;
    QPushButton *traj_exit_teleop_btn_;

    QPushButton *drone_arm_btn_;
    QPushButton *drone_get_auth_btn_;
    QPushButton *drone_release_auth_btn_;
    bool drone_armed;

    //Go Button
    QPushButton *traj_go_btn_;

    // One-line text editor for entering the outgoing ROS topic name.
    QLineEdit* output_topic_editor_;
    float defaultHeight;

    // The current name of the output topic.
    QString output_topic_;

    TrajPointsDataModel *table_view_model_;
    QTableView *table_view_;

    WaypointNavigatorParams wn_params;

    // The ROS node handle.
    ros::NodeHandle nh_;
    tf::TransformListener tf_listner_;

    ros::Subscriber rviz_nav_goal_sub_;
    ros::Subscriber dji_flight_status_sub_;

    ros::ServiceClient srv_vis_traj_client_;
    ros::ServiceClient srv_execute_traj_client_;
    ros::ServiceClient srv_exit_teleop_client_;
    ros::ServiceClient srv_drone_arm_client_;
    ros::ServiceClient srv_sdk_authority_client_;
    bool dji_sdk_has_authority;

    // END_TUTORIAL
    void rvizPointHndler(const geometry_msgs::PoseStampedConstPtr &posePtr);

    void updateArmButton();
    void updateSdkAuthorityButtons();
    void dji_flight_status(const std_msgs::UInt8ConstPtr &statusPtr);
};

} // end namespace rviz_plugin_traj_planner

#endif // TRAJECTORY_PLUGIN_H
