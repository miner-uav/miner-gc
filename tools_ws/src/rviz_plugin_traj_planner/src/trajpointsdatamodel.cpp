#include "trajpointsdatamodel.h"

#include <ros/ros.h>

namespace rviz_plugin_traj_planner
{

TrajPointsDataModel::TrajPointsDataModel(QObject *parent):
    QAbstractTableModel(parent)
{

}

int TrajPointsDataModel::rowCount(const QModelIndex &parent) const
{
    return this->traj_points_.size();
}

int TrajPointsDataModel::columnCount(const QModelIndex &parent) const
{
    return TableColumnsId_COUNT;
}

QVariant TrajPointsDataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal) {
            switch (section)
            {
            case TableColumnsId_POS_X:
                return QString("X [m]");
            case TableColumnsId_POS_Y:
                return QString("Y [m]");
            case TableColumnsId_POS_Z:
                return QString("Z [m]");
            case TableColumnsId_HEAD:
                return QString("Heading [deg]");
            default:
                return QString("Unknown Column");
            }
        }
    }
    return QVariant();
}

QVariant TrajPointsDataModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
    {
       return QVariant();
    }

    const TrajPoint *thisPoint = this->traj_points_[index.row()];
    switch (index.column()) {
    case TableColumnsId_POS_X:
        return thisPoint->x;
        break;
    case TableColumnsId_POS_Y:
        return thisPoint->y;
        break;
    case TableColumnsId_POS_Z:
        return thisPoint->z;
        break;
    case TableColumnsId_HEAD:
        return thisPoint->heading;
        break;
    default:
        ROS_ERROR("data read - Invalid column :%d", index.column());
        break;
    }
}

bool TrajPointsDataModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (index.isValid() && role == Qt::EditRole)
    {
        bool operationResult = true;
        TrajPoint *thisPoint = traj_points_[index.row()];
        switch (index.column()) {
        case TableColumnsId_POS_X:
            thisPoint->x = value.toDouble();
            break;
        case TableColumnsId_POS_Y:
            thisPoint->y = value.toDouble();
            break;
        case TableColumnsId_POS_Z:
            thisPoint->z = value.toDouble();
            break;
        case TableColumnsId_HEAD:
            thisPoint->heading = value.toDouble();
            break;
        default:
            operationResult = false;
            ROS_ERROR("data write - Invalid column ID:%d", index.column());
            break;
        }

        Q_EMIT dataChanged(index, index);
        Q_EMIT updateTrajectory();
        return operationResult;
    }
    return false;
}

bool TrajPointsDataModel::insertRows(int row, int count, const QModelIndex &parent)
{
    beginInsertRows(QModelIndex(), row, row + count - 1);

    for (int row = 0; row < count; ++row)
    {
        traj_points_.insert(row, new TrajPoint());
    }

    endInsertRows();
    return true;
}

bool TrajPointsDataModel::removeRows(int row, int count, const QModelIndex &parent)
{
    beginRemoveRows(QModelIndex(), row, row + count - 1);

    for (int i = 0; i < count; i++)
    {
        traj_points_.removeAt(row + i);
    }

    endRemoveRows();
    Q_EMIT updateTrajectory();
    return true;
}

Qt::ItemFlags TrajPointsDataModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
    {
            return Qt::ItemIsEnabled;
    }
    return Qt::ItemIsEditable | QAbstractTableModel::flags(index);
}

void TrajPointsDataModel::clearData()
{
    removeRows(0,traj_points_.size(), QModelIndex());
}

void TrajPointsDataModel::exportData(std::vector<double> &northings, std::vector<double> &eastings, std::vector<double> &heights, std::vector<double> &headings)
{
    //northings.push_back(current_pose_.y);
    //eastings.push_back(current_pose_.x);
    //heights.push_back(current_pose_.z);
    //headings.push_back(current_pose_.heading);
    for(auto item : traj_points_)
    {
        northings.push_back(item->y);
        eastings.push_back(item->x);
        heights.push_back(item->z);
        headings.push_back(item->heading);
    }
}

void TrajPointsDataModel::addTrajPoint(double x, double y, double z, double heading )
{
    int lastRow = traj_points_.size()-1;
    if(lastRow < 0 )lastRow = 0;

    TrajPoint *point = new TrajPoint(  x
                                     , y
                                     , z
                                     , heading
                                     );

    beginInsertRows(QModelIndex(), lastRow+1, lastRow+1);
    traj_points_.push_back(point);
    endInsertRows();
}

}
