#ifndef TRAJPOINTSDATAMODEL_H
#define TRAJPOINTSDATAMODEL_H

#include <QAbstractTableModel>
#include <QString>
namespace rviz_plugin_traj_planner
{

enum TableColumnsIds
{
    TableColumnsId_POS_X = 0,
    TableColumnsId_POS_Y,
    TableColumnsId_POS_Z,
    TableColumnsId_HEAD,
    TableColumnsId_COUNT
};

class TrajPoint
{
public:
    TrajPoint(){}
    TrajPoint(double x, double y, double z, double heading):
       x(x)
      ,y(y)
      ,z(z)
      ,heading(heading)
    {
    }

    double x;
    double y;
    double z;
    double heading;
};

class TrajPointsDataModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    TrajPointsDataModel(QObject *parent = 0);

    // QAbstractItemModel interface
    int rowCount(const QModelIndex &parent) const override;
    int columnCount(const QModelIndex &parent) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;
    bool insertRows(int row, int count, const QModelIndex &parent) override;
    bool removeRows(int row, int count, const QModelIndex &parent) override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;

    void clearData();
    void exportData(std::vector<double>&northings, std::vector<double>&eastings, std::vector<double>&heights, std::vector<double>&headings);

    void addTrajPoint(double x, double y, double z, double heading );
Q_SIGNALS:
  void updateTrajectory();

private:
    QList<TrajPoint*> traj_points_;


};

}
#endif // TRAJPOINTSDATAMODEL_H
