Calibration results
===================
Normalized Residuals
----------------------------
Reprojection error (cam0):     mean 2.02580054044, median 1.98274210957, std: 0.992694645678
Reprojection error (cam1):     mean 2.06467891795, median 1.96030812582, std: 1.33507849613
Gyroscope error (imu0):        mean 2.85995194787, median 1.84965217859, std: 3.96718066025
Accelerometer error (imu0):    mean 2.73019145363, median 2.00369751696, std: 2.57422227175

Residuals
----------------------------
Reprojection error (cam0) [px]:     mean 2.02580054044, median 1.98274210957, std: 0.992694645678
Reprojection error (cam1) [px]:     mean 2.06467891795, median 1.96030812582, std: 1.33507849613
Gyroscope error (imu0) [rad/s]:     mean 0.00360801056148, median 0.00233345339959, std: 0.00500484972559
Accelerometer error (imu0) [m/s^2]: mean 0.0864880527077, median 0.0634738989556, std: 0.0815472011038

Transformation (cam0):
-----------------------
T_ci:  (imu0 to cam0): 
[[ 0.00380769 -0.99943285  0.03345862  0.04204647]
 [-0.99996483 -0.00405548 -0.00734109 -0.00530267]
 [ 0.00747262 -0.03342949 -0.99941314 -0.00992963]
 [ 0.          0.          0.          1.        ]]

T_ic:  (cam0 to imu0): 
[[ 0.00380769 -0.99996483  0.00747262 -0.00538838]
 [-0.99943285 -0.00405548 -0.03342949  0.04166918]
 [ 0.03345862 -0.00734109 -0.99941314 -0.01136954]
 [ 0.          0.          0.          1.        ]]

timeshift cam0 to imu0: [s] (t_imu = t_cam + shift)
0.00847213088816


Transformation (cam1):
-----------------------
T_ci:  (imu0 to cam1): 
[[ 0.00747534 -0.99939891  0.03385166 -0.05834059]
 [-0.99996944 -0.00754851 -0.00203429 -0.00564961]
 [ 0.0022886  -0.03383542 -0.9994248  -0.0117885 ]
 [ 0.          0.          0.          1.        ]]

T_ic:  (cam1 to imu0): 
[[ 0.00747534 -0.99996944  0.0022886  -0.00518635]
 [-0.99939891 -0.00754851 -0.03383542 -0.05874703]
 [ 0.03385166 -0.00203429 -0.9994248  -0.00981829]
 [ 0.          0.          0.          1.        ]]

timeshift cam1 to imu0: [s] (t_imu = t_cam + shift)
0.00840959882628

Baselines:
----------
Baseline (cam0 to cam1): 
[[ 0.9999932  -0.00367054 -0.00036654 -0.10040988]
 [ 0.00366859  0.99997982 -0.00518695 -0.00055281]
 [ 0.00038557  0.00518557  0.99998648 -0.00184772]
 [ 0.          0.          0.          1.        ]]
baseline norm:  0.10042839811 [m]


Gravity vector in target coords: [m/s^2]
[-0.48314773 -9.78749354 -0.37411421]


Calibration configuration
=========================

cam0
-----
  Camera model: pinhole
  Focal length: [461.4813339294591, 462.58263322993736]
  Principal point: [385.2197574052821, 220.68859003594648]
  Distortion model: radtan
  Distortion coefficients: [-0.3622260518942729, 0.09980665165768134, 0.0007282961459409154, -0.0025324909468661958]
  Type: aprilgrid
  Tags: 
    Rows: 6
    Cols: 6
    Size: 0.0875 [m]
    Spacing 0.02625 [m]


cam1
-----
  Camera model: pinhole
  Focal length: [461.47115258890796, 461.9894603273526]
  Principal point: [351.0773748700476, 233.15278476893172]
  Distortion model: radtan
  Distortion coefficients: [-0.3543844121346264, 0.09082907187863073, 0.00039784458486118824, -0.0027784291509307638]
  Type: aprilgrid
  Tags: 
    Rows: 6
    Cols: 6
    Size: 0.0875 [m]
    Spacing 0.02625 [m]



IMU configuration
=================

IMU0:
----------------------------
  Model: calibrated
  Update rate: 200
  Accelerometer:
    Noise density: 0.00224 
    Noise density (discrete): 0.0316783837972 
    Random walk: 7.53e-05
  Gyroscope:
    Noise density: 8.9206e-05
    Noise density (discrete): 0.00126156335045 
    Random walk: 1.08e-05
  T_i_b
    [[ 1.  0.  0.  0.]
     [ 0.  1.  0.  0.]
     [ 0.  0.  1.  0.]
     [ 0.  0.  0.  1.]]
  time offset with respect to IMU0: 0.0 [s]

