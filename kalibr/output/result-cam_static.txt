Calibration results 
====================
Camera-system parameters:
	cam0 (/cam0/image_raw):
	 type: <class 'aslam_cv.libaslam_cv_python.DistortedPinholeCameraGeometry'>
	 distortion: [-0.36222605  0.09980665  0.0007283  -0.00253249] +- [ 0.00060558  0.00045249  0.00010829  0.00007509]
	 projection: [ 461.48133393  462.58263323  385.21975741  220.68859004] +- [ 0.30494845  0.30432997  0.31577588  0.35677646]
	 reprojection error: [-0.000017, -0.000001] +- [0.398545, 0.296905]

	cam1 (/cam1/image_raw):
	 type: <class 'aslam_cv.libaslam_cv_python.DistortedPinholeCameraGeometry'>
	 distortion: [-0.35438441  0.09082907  0.00039784 -0.00277843] +- [ 0.00053491  0.00036467  0.00010741  0.00007387]
	 projection: [ 461.47115259  461.98946033  351.07737487  233.15278477] +- [ 0.30001755  0.30013739  0.29893324  0.35535717]
	 reprojection error: [0.000016, 0.000003] +- [0.480630, 0.363280]

	baseline T_1_0:
	 q: [-0.00259314  0.00018803 -0.00183479  0.99999494] +- [ 0.0005079   0.00069112  0.00007053]
	 t: [-0.10040988 -0.00055281 -0.00184772] +- [ 0.00008888  0.00008697  0.00022803]



Target configuration
====================

  Type: aprilgrid
  Tags: 
    Rows: 6
    Cols: 6
    Size: 0.0875 [m]
    Spacing 0.02625 [m]
