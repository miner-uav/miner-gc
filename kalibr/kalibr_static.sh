#!/usr/bin/env bash

BAG_FILE=/data/rosbag/vi_sensor_calib/calibr_static.bag
KALIBR_WS=/home/maciej/kalibr_workspace

CAMERA_TOPICS='/cam0/image_raw /cam1/image_raw'



source $KALIBR_WS/devel/setup.bash


rm -f camchain*static*
rm -f report-cam*static*.pdf
rm -f results-cam*static*.txt
rm -rf output/*
mkdir -p output

#run static calibrarion
echo kalibr_calibrate_cameras --bag $BAG_FILE --topics $CAMERA_TOPICS --models pinhole-radtan pinhole-radtan --target april_6x6_80x80cm.yaml --show-extraction 




mv `ls -N camchain*static*`  output/camchain_static.yaml
mv `ls -N report-cam*static*.pdf`  output/report-cam_static.pdf
mv `ls -N results-cam*static*.txt`  output/result-cam_static.txt
