#!/usr/bin/env bash

BAG_FILE=/data/rosbag/vi_sensor_calib/calibr_dynamic.bag
KALIBR_WS=/home/maciej/kalibr_workspace

CAMERA_TOPICS='/cam0/image_raw /cam1/image_raw'
CAM_YAML=output/camchain_static.yaml
IMU_YAML=imu_pixhawk.yaml

source $KALIBR_WS/devel/setup.bash

rm -f *dynamic*.pdf
rm -f *dynamic*.txt
rm -f *dynamic*.yaml



#run dynamic calibration
kalibr_calibrate_imu_camera \
--bag $BAG_FILE \
--cam $CAM_YAML \
--imu $IMU_YAML \
--target april_6x6_80x80cm.yaml \
--show-extraction \
--time-calibration

mv `ls -N camchain*.yaml`  output/camchain_dynamic.yaml
mv `ls -N report-imucam*dynamic*.pdf`  output/report-cam_dynamic.pdf
mv `ls -N results-imucam*dynamic*.txt`  output/result-cam_dynamic.txt

#convert to rovio format
$KALIBR_WS/src/Kalibr/aslam_offline_calibration/kalibr/python/exporters/kalibr_rovio_config \
--cam output/camchain_dynamic.yaml

mv rovio* output/
