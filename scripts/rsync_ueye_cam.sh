#!/usr/bin/env bash
SRC=/data/git/miner-rovio/catkin_ws/src
rsync -av \
--exclude=.git/* \
--exclude=*.user \
--exclude=*.orig \
$SRC/ueye_cam/ odroid@192.168.128.23:$SRC/ueye_cam 
