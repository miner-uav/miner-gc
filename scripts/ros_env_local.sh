#!/usr/bin/env bash
source /opt/ros/indigo/setup.bash
source /data/git/miner-rovio/catkin_ws/devel/setup.bash

export ROS_MASTER_URI=http://127.0.0.1:11311/
export ROS_IP=127.0.0.1
export ROS_HOSTNAME=127.0.0.1
