#!/usr/bin/env sh

REMOTE_HOST=192.168.8.1
echo connecting 192.168.8.1
sshfs -o idmap=user ubuntu@$REMOTE_HOST:/home/ubuntu /mnt/tx2/home -o reconnect -C -o workaround=all 
sshfs -o idmap=user ubuntu@$REMOTE_HOST:/data /mnt/tx2/data -o reconnect -C -o workaround=all 
