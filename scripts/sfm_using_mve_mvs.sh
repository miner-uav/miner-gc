#!/usr/bin/env bash
# This script runs a series of steps to turn images into a coloured and textured pointcloud
# https://wiki.csiro.au/display/MineInformatics/scripts+to+produce+textured+3D

set -e
set -u

# Point to compiled source code
makescene="$HOME/git/mve/apps/makescene/makescene"
sfmrecon="$HOME/git/mve/apps/sfmrecon/sfmrecon"
dmrecon="$HOME/git/mve/apps/dmrecon/dmrecon"
scene2pset="$HOME/git/mve/apps/scene2pset/scene2pset"
fssrecon="$HOME/git/mve/apps/fssrecon/fssrecon"
meshclean="$HOME/git/mve/apps/meshclean/meshclean"
texrecon="$HOME/git/mvs-texturing/build/apps/texrecon/texrecon"

# Start SfM pipeline using images in the current directory
$makescene -i . scene
$sfmrecon scene
$dmrecon -s3 scene
$scene2pset -F3 scene point-set.ply
$fssrecon point-set.ply surface.ply
$meshclean -t10 -c10000 surface.ply surface-clean.ply
$texrecon scene::undistorted surface-clean.ply textured
