#!/usr/bin/env sh

sudo ps -C sshfs -o pid= | awk '{system("kill -9 " $1)}'
sudo umount /mnt/tx2/home
sudo umount /mnt/tx2/data
