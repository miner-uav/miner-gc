#!/usr/bin/env bash
source /opt/ros/indigo/setup.bash
source /data/git/miner-rovio/catkin_ws/devel/setup.bash

export ROS_MASTER_URI=http://192.168.128.23:11311/
export ROS_IP=192.168.128.21
export ROS_HOSTNAME=192.168.128.21
