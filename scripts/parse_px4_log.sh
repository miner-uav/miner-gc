#!/usr/bin/env sh
set -e 
echo running...
# setup
PX4_FIRMWARE=/data/git/Firmware
SDLOG_DUMP=$PX4_FIRMWARE/Tools/sdlog2/sdlog2_dump.py

SOURCE_DIR=$1
DEST_FOLDER=$(dirname "${SOURCE_FILE}")
DEST_FILE=${SOURCE_FILE%.*}.csv
PYTHON=python

if [ -z "$SOURCE_DIR" ]; then 
	echo "Ussage $0 <LOGS_DIR_NAME>"
	exit 1
fi

if [ ! -d $SOURCE_DIR ]; then
    echo "ERROR: Directory: $SOURCE_DIR - does not exists or is not a dir"  
    echo "Ussage $0 <LOGS_DIR_NAME>"
    exit 2
fi

# command line
for px4log_file in $SOURCE_DIR/*.px4log; do
    echo $px4log_file
    DEST_FILE=${px4log_file%.*}.csv
    if [ -f $DEST_FILE ];
	then
	   echo "File $DEST_FILE exists - skipping"
	else
	   $PYTHON $SDLOG_DUMP $px4log_file > $DEST_FILE
	fi
done

#$PYTHON $SDLOG_DUMP $SOURCE_FILE > $DEST_FILE

