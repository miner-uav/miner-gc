#!/usr/bin/env bash
# This script sets up environment variables on the Ground Control System PC when interfacing with roscore on the quadcopter.

#set -e
#set -u

# Initialize defaults:
#LOCAL_IP=$(ifconfig wlan1 2>/dev/null|awk '/inet addr:/ {print $2}'|sed 's/addr://')
#ROS_MASTER_IP="192.168.66.192"

LOCAL_IP="192.168.66.201"
ROS_MASTER_IP="192.168.66.154"
#ROS_MASTER_IP="140.79.82.25"

# The command line help
show_help() {
    echo "Usage: $0 [option...]" >&2
    echo "    -l    local IP address (default is the current IP address of WLAN1 = $LOCAL_IP)"
    echo "    -m    IP address where ROS master is (default = $ROS_MASTER_IP)"
    exit 1
}

# option parsing based on - http://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash
# A POSIX variable
OPTIND=1         # Reset in case getopts has been used previously in the shell.

##echo "Parsed opts"
while getopts "h:?:l:m:" opt; do
    case "$opt" in
    h|\?)
        show_help
        exit 0
        ;;
    l)  LOCAL_IP=$OPTARG
##        echo "    LOCAL_IP=$LOCAL_IP"
        ;;
    m)  ROS_MASTER_IP=$OPTARG
##        echo "    ROS_MASTER_IP=$ROS_MASTER_IP"
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift
##echo "    Leftovers: $@"

source /opt/ros/indigo/setup.bash
source /home/admin/git/ams-rovio-ws/catkin_ws/devel/setup.bash

# These environment variables are set to allow access via command and control PC
export ROS_MASTER_URI=http://$ROS_MASTER_IP:11311/
export ROS_IP=$LOCAL_IP
export ROS_HOSTNAME=$LOCAL_IP
echo "Set environment variables"
echo "    ROS_MASTER_URI=$ROS_MASTER_URI"
echo "    ROS_IP=$ROS_IP"
echo "    ROS_HOSTNAME=$ROS_HOSTNAME"
