#!/usr/bin/env sh

REMOTE_HOST=192.168.128.32
echo connecting $REMOTE_HOST
sshfs -o idmap=user ubuntu@$REMOTE_HOST:/home/ubuntu /mnt/tx2/home -o reconnect -C -o workaround=all 
sshfs -o idmap=user ubuntu@$REMOTE_HOST:/data /mnt/tx2/data -o reconnect -C -o workaround=all 
