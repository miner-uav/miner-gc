#!/usr/bin/env bash

DATE_STAMP=`date +%Y-%m-%d.%H:%M:%S`
OUT_FOLDER=/home/maciej/Dropbox/Drone/Source
REPO_BASE_PATH=/data/git


archive_repo(){
	REPO_NAME=$1
	BRANCH=$2
	OUT_FILE=${OUT_FOLDER}/${REPO_NAME}_${BRANCH}_${DATE_STAMP}.zip
	echo "Archiving repo:${REPO_NAME} ; branch: ${BRANCH} to ${OUT_FILE}"
	git -C ${REPO_BASE_PATH}/${REPO_NAME} archive \
		--format=zip \
		--prefix=${BRANCH}/ \
		${BRANCH} \
		> ${OUT_FILE}
	echo "Archiving DONE"
}

archive_repo miner-tx1 eth_mpc
archive_repo miner-explorer master
archive_repo miner-cad master
archive_repo miner-maplab master
archive_repo miner-ros-camera master
archive_repo miner-nvidia-linux master
