#!/usr/bin/env bash
THIS_PATH=`dirname ${BASH_SOURCE[0]}`

cp $THIS_PATH/ros_env_local.sh ~/ros_env.sh
chmod +x ~/ros_env.sh

echo environment is set :
source ~/ros_env.sh
export ROS_MASTER_URI=$ROS_MASTER_URI
export ROS_IP=$ROS_IP
export ROS_HOSTNAME=$ROS_HOSTNAME
echo ROS_IP:$ROS_IP
echo ROS_MASTER_URI:$ROS_MASTER_URI
echo ROS_HOSTNAME:$ROS_HOSTNAME

echo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
echo remember to  soutce it: source ~/ros_env.sh

